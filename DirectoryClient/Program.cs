﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Linq.Expressions;
using Grpc.Core;
using Directory.V1;

namespace DirectoryClient
{
    class Program
    {
        public static void Main(string[] args)
        {
            Channel channel = new Channel("localhost:30051", ChannelCredentials.Insecure);

            var client = new DirectoryAPI.DirectoryAPIClient(channel);

            client.AddPerson(new AddPersonRequest {
                    Person = new Person {
                        FirstName = "John",
                        Surname = "Doe",
                        DateOfBirth = new Date {
                            Day = 23,
                            Month = 8,
                            Year = 1984,
                        },
                        Address = new Address {
                            HouseNumber = 54,
                            Street = "Souterhead Road",
                            Postcode = "NR12 4AZ",
                        },
                    }
                });

            client.AddPerson(new AddPersonRequest {
                    Person = new Person {
                        FirstName = "Jane",
                        Surname = "Doe",
                        DateOfBirth = new Date {
                            Day = 31,
                            Month = 3,
                            Year = 1984,
                        }
                    }
                });

            Console.WriteLine("Welcome to the API Test Client. The client supports Add, List, Delete and Exit commands.");

            var awaitingCommand = true;
            while (awaitingCommand)
            {
                Console.Write("Waiting for command : ");
                awaitingCommand = HandleCommand(Console.ReadLine(), client);
            }

            channel.ShutdownAsync().Wait();
        }

        private static bool HandleCommand(string command, DirectoryAPI.DirectoryAPIClient client)
        {
            switch (command.ToLower())
            {
                case "exit": return false;
                case "add":
                    HandleAdd(client);
                    break;
                case "list":
                    HandleList(client);
                    break;
                case "delete":
                    HandleDelete(client);
                    break;
            }

            return true;
        }

        private static void HandleDelete(DirectoryAPI.DirectoryAPIClient client)
        {
            Console.Write("Please enter unique person id : ");
            var id = Console.ReadLine();
            ///do something with the client to delete the person
        }

        private static void HandleList(DirectoryAPI.DirectoryAPIClient client)
        {
            var resp = client.ListPeople(new ListPeopleRequest());

            Console.WriteLine("Listing Directory");
            foreach (var person in resp.People) {
                Console.WriteLine(person.ToString());
            }
        }

        private static void HandleAdd(DirectoryAPI.DirectoryAPIClient client)
        {
            Console.Write("Please enter first name : ");
            var firstName = Console.ReadLine();
            Console.Write("Please enter last name : ");
            var lastName = Console.ReadLine();
            Console.Write("Please enter D.O.B as DD/MM/YYYY : ");
            var dob = Console.ReadLine();
            var dateOfBirth = DateTime.ParseExact(dob, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            client.AddPerson(new AddPersonRequest {
                Person = new Person {
                    FirstName = firstName,
                    Surname = lastName,
                    DateOfBirth = new Date {
                        Day = dateOfBirth.Day,
                        Month = dateOfBirth.Month,
                        Year = dateOfBirth.Year,
                    }
                }
            });
        }
    }
}

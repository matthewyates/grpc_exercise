﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Grpc.Core;
using Directory.V1;

namespace DirectoryServer
{
    class DirectoryImpl : DirectoryAPI.DirectoryAPIBase
    {
        public DirectoryImpl()
        {
            people = new List<Person>();
        }

        public override Task<AddPersonResponse> AddPerson(AddPersonRequest request, ServerCallContext context)
        {
            if (request.Person.Id != "") {
                throw new RpcException(new Status(StatusCode.InvalidArgument, "the field 'id' should not be provided by the client"));
            }

            Guid id = Guid.NewGuid();

            request.Person.Id = id.ToString();

            people.Add(request.Person);

            return Task.FromResult(new AddPersonResponse {Person = request.Person});
        }

        public override Task<ListPeopleResponse> ListPeople(ListPeopleRequest request, ServerCallContext context)
        {
                throw new RpcException(new Status(StatusCode.Unimplemented, "ListPeople unimplemented"));
        }

        private List<Person> people;
    }

    class Program
    {
        const int Port = 30051;

        public static void Main(string[] args)
        {
            Server server = new Server {
                Services = { DirectoryAPI.BindService(new DirectoryImpl()) },
                Ports = { new ServerPort("localhost", Port, ServerCredentials.Insecure) }
            };
            server.Start();

            Console.WriteLine("Greeter server listening on port " + Port);
            Console.WriteLine("Press any key to stop the server...");
            Console.ReadKey();

            server.ShutdownAsync().Wait();
        }
    }
}

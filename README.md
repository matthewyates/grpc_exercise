Exercise 1
==========

In the api directory there is a very simple Directory service with two methods,
`AddPerson` and `ListPeople`. The `AddPerson` has already been implemented in
the `DirectoryServer` project - implement directory.v1.DirectoryAPI.ListPeople.

Note, the server will store the directory in memory. Persistant storage is not
required for this exercise.

Exercise 2
==========

Add a field of type `string` to hold the phone number of the person in the
directory.

Exercise 3
==========

The API allows additions to the Directory but there is no method to remove them.
Add the method `RemovePerson` to the `.proto` file and then implement.

A person is uniquely identifyable by a UUID that is generated in AddPerson
implementation. This is what should be used to identify which person should be
removed.

Exercise 4
==========

Deprecate the fields `first_name` and `surname` and create a new message
called FullName which supports middle names and titles (e.g Mr, Ms, Dr etc...).
Implement this in the server without breaking compatibility with existing clients.

Note, an `Enum` type could be sutiable for the title field.

Also be aware that the server implementation should still support requests that
use the deprecated fields, so you might have to modify the `AddPeson` method.

﻿using System;
using Grpc.Core;
using Directory.V1;

namespace DirectoryClient
{
    class Program
    {
        public static void Main(string[] args)
        {
            Channel channel = new Channel("localhost:30051", ChannelCredentials.Insecure);

            var client = new DirectoryAPI.DirectoryAPIClient(channel);

            client.AddPerson(new AddPersonRequest {
                    Person = new Person {
                        FirstName = "John",
                        Surname = "Doe",
                        DateOfBirth = new Date {
                            Day = 23,
                            Month = 8,
                            Year = 1984,
                        },
                        Address = new Address {
                            HouseNumber = 54,
                            Street = "Souterhead Road",
                            Postcode = "NR12 4AZ",
                        },
                    }
                });

            client.AddPerson(new AddPersonRequest {
                    Person = new Person {
                        FirstName = "Jane",
                        Surname = "Doe",
                        DateOfBirth = new Date {
                            Day = 31,
                            Month = 3,
                            Year = 1984,
                        }
                    }
                });

            var resp = client.ListPeople(new ListPeopleRequest());

            Console.WriteLine("Listing Directory");
            foreach (var person in resp.People) {
                Console.WriteLine(person.ToString());
            }

            channel.ShutdownAsync().Wait();
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}
